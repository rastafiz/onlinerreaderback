package com.reader.onliner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages ="com.reader.onliner")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(new Class<?>[]{Application.class}, args);
    }
}
