package com.reader.onliner.controller;

import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.reader.onliner.entity.ForumSection;
import com.reader.onliner.service.SectionService;

/**
 * Created by aliakseimitskevich on 10/14/15.
 */
@RestController
public class SectionController
{

  public static final String FORUM_ONLINER_BY_MAIN_PAGE_URL = "http://forum.onliner.by/";

  @Autowired
  private SectionService sectionService;

  @RequestMapping("/sections")
  @ResponseBody
  public List<ForumSection> sectionList()
  {
    Document doc = null;
    try
    {
      doc = Jsoup.connect(FORUM_ONLINER_BY_MAIN_PAGE_URL).get();
      List<ForumSection> sectionsList = sectionService.createSectionsList(doc);
      return sectionsList;
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    return null;
  }
}
