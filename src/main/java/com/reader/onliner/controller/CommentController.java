package com.reader.onliner.controller;

import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reader.onliner.entity.ForumComment;
import com.reader.onliner.entity.ForumCommentsPage;
import com.reader.onliner.entity.ForumConstants;
import com.reader.onliner.service.CommentService;
import com.reader.onliner.utils.CommonForumUtils;

/**
 * Created by aliakseimitskevich on 10/15/15.
 */
@RestController
public class CommentController implements ForumConstants {

    @Autowired
    private CommentService commentService;

    @RequestMapping("/comments/{threadId}/{currentPage}")
    public ForumCommentsPage getComments(@PathVariable Long threadId, @PathVariable Long currentPage) {
        try {
            if (currentPage == null) {
                currentPage = 1L;
            }
            String start = String.valueOf(currentPage * 20 - 20);
            String url = VIEW_THREAD_PREFIX + threadId + "&start=" + start;
            Document doc = Jsoup.connect(url).get();
            Long totalPages = CommonForumUtils.getTotalPage(doc);
            Boolean isClosed = isClosed(doc);
            List<ForumComment> comments = commentService.getComments(doc);
            return new ForumCommentsPage(isClosed, currentPage, totalPages, comments);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Boolean isClosed(Document doc) {
        Elements status = doc.select("div.b-hdtopic > span.btn-hdtopic-lft > span.topic-closed");
        return status.size() > 0;
    }
}
