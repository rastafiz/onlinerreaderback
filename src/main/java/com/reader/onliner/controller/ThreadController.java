package com.reader.onliner.controller;

import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reader.onliner.entity.ForumConstants;
import com.reader.onliner.entity.ForumThread;
import com.reader.onliner.entity.ForumThreadsPage;
import com.reader.onliner.service.ThreadService;
import com.reader.onliner.utils.CommonForumUtils;

/**
 * Created by aliakseimitskevich on 10/15/15.
 */
@RestController
public class ThreadController implements ForumConstants {

    @Autowired
    private ThreadService threadService;

    @RequestMapping("/threads/{subsectionId}/{currentPage}")
    public ForumThreadsPage getForumThread(@PathVariable Integer subsectionId, @PathVariable Long currentPage) {
        try {
            if (currentPage == null) {
                currentPage = 1L;
            }
            String start = String.valueOf(currentPage * 50 - 50);
            String url = VIEW_FORUM_PREFIX + subsectionId + "&start=" + start;
            Document doc = Jsoup.connect(url).get();
            Long totalPages = CommonForumUtils.getTotalPage(doc);
            List<ForumThread> threads = threadService.getThreads(doc);
            return new ForumThreadsPage(currentPage, totalPages, threads);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
