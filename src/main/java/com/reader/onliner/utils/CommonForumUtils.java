package com.reader.onliner.utils;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by aliakseimitskevich on 10/15/15.
 */
public class CommonForumUtils {


    public static Long getTotalPage(Document doc) {
        Elements pages = doc.select("ul.pages-fastnav > li > a");
        Long totalPages = 1L;
        if (pages.size() != 0) {
            if ("".equals(pages.last().text())) {
                totalPages = Long.valueOf(pages.get(pages.size() - 2).text());
            } else {
                totalPages = Long.valueOf(pages.get(pages.size() - 1).text());
            }
        }
        return totalPages;
    }

    public static Long getMillisFromDate(String date) {
        Locale locale = new Locale("ru");
        String[] a = date.split(" ");
        String currentMonth = a[1];
        String cutMonth = getCorrectMonth(currentMonth);
//        String cutMonth = currentMonth.substring(0, currentMonth.length() - 1);
        a[1] = cutMonth;

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < a.length; i++) {
            sb.append(a[i]).append(" ");
        }
        String resultDate = sb.toString().trim();
        SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy HH:mm", locale);
        Date d = null;
        try {
            d = format.parse(resultDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d.getTime();
    }


    private static String getCorrectMonth(String currentMonth) {
        String cutMonth;
        if (currentMonth.equals("января")) {
            cutMonth = "январь";
        } else if (currentMonth.equals("февраля")) {
            cutMonth = "февраль";
        } else if (currentMonth.equals("марта")) {
            cutMonth = "март";
        } else if (currentMonth.equals("апреля")) {
            cutMonth = "апрель";
        } else if (currentMonth.equals("мая")) {
            cutMonth = "май";
        } else if (currentMonth.equals("июня")) {
            cutMonth = "июнь";
        } else if (currentMonth.equals("июля")) {
            cutMonth = "июль";
        } else if (currentMonth.equals("августа")) {
            cutMonth = "август";
        } else if (currentMonth.equals("сентября")) {
            cutMonth = "сентябрь";
        } else if (currentMonth.equals("октября")) {
            cutMonth = "октябрь";
        } else if (currentMonth.equals("ноября")) {
            cutMonth = "ноябрь";
        } else if (currentMonth.equals("декабря")) {
            cutMonth = "декабрь";
        } else {
            cutMonth = "Invalid month";
        }
        return cutMonth;
    }

}
