package com.reader.onliner.entity;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 10.12.13
 * Time: 19:31
 * To change this template use File | Settings | File Templates.
 */

@Component
public class ForumSubsection implements ForumConstants {
    private static final String SUBSECTIONS_LIST = "div.b-hdtopic ~ ul.b-list-topics";
    private static final String HOT_SUBSECTION_TITLES = "li > h3 > a";
    private static final String SUBSECTION_TITLES = "li > div > h3 > a";
    private static final String SUBSECTION_DESCRIPTIONS = "li > div > p";
    private static final String THREAD_COUNTS = "li > div > div > strong";
    private static final String COMMENT_COUNTS = "li > div > div";
    private static final String LAST_COMMENT_AUTHOR_NAMES = "li > div.b-lt-author > a.dark-gray";
    private static final String LAST_COMMENT_CREATION_DATES = "li > div.b-lt-author > a.link-getlast";

    private ForumSubsection parentSection;
    private String title;
    private String description;
    private Long subsectionId;
    private Integer threadsCount;
    private Integer commentsCount;
    private Long lastCommentCreationDate;
    private String lastCommentAuthorId;

    public ForumSubsection() {
    }

    public ForumSubsection(ForumSubsection parentSection, String title, String description, Long subsectionId, Integer threadsCount,
                           Integer commentsCount, Long lastCommentCreationDate, String lastCommentAuthorId) {
        this.parentSection = parentSection;
        this.title = title;
        this.description = description;
        this.subsectionId = subsectionId;
        this.threadsCount = threadsCount;
        this.commentsCount = commentsCount;
        this.lastCommentCreationDate = lastCommentCreationDate;
        this.lastCommentAuthorId = lastCommentAuthorId;
    }

    public ForumSubsection getParentSection() {
        return parentSection;
    }

    public void setParentSection(ForumSubsection parentSection) {
        this.parentSection = parentSection;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSubsectionId() {
        return subsectionId;
    }

    public void setSubsectionId(Long subsectionId) {
        this.subsectionId = subsectionId;
    }

    public Integer getThreadsCount() {
        return threadsCount;
    }

    public void setThreadsCount(Integer threadsCount) {
        this.threadsCount = threadsCount;
    }

    public Integer getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(Integer commentsCount) {
        this.commentsCount = commentsCount;
    }

    public Long getLastCommentCreationDate() {
        return lastCommentCreationDate;
    }

    public void setLastCommentCreationDate(Long lastCommentCreationDate) {
        this.lastCommentCreationDate = lastCommentCreationDate;
    }

    public String getLastCommentAuthorId() {
        return lastCommentAuthorId;
    }

    public void setLastCommentAuthorId(String lastCommentAuthorId) {
        this.lastCommentAuthorId = lastCommentAuthorId;
    }

    public static void completeSectionsSubsections(Document doc, List<ForumSection> sectionsList) {
        Elements elements;
        elements = doc.select(SUBSECTIONS_LIST);
        int i = 0;
        for (Element element : elements) {
            List<ForumSubsection> subsections = new ArrayList<ForumSubsection>();
            Elements hotSubsectionTitles = element.select(HOT_SUBSECTION_TITLES);
            subsections = getHotSubsections(hotSubsectionTitles);
            if (subsections.size() == 0) {
                Elements subsectionsTitles = element.select(SUBSECTION_TITLES);
                Elements subsectionsDescriptions = element.select(SUBSECTION_DESCRIPTIONS);
                Elements threadCounts = element.select(THREAD_COUNTS);
                Elements commentsCounts = element.select(COMMENT_COUNTS);
                Elements lastCommentAuthorNames = element.select(LAST_COMMENT_AUTHOR_NAMES);
                Elements lastCommentCreationDate = element.select(LAST_COMMENT_CREATION_DATES);
                subsections = getSubsections(subsectionsTitles, subsectionsDescriptions, threadCounts, commentsCounts, lastCommentAuthorNames,
                        lastCommentCreationDate);
            }
            sectionsList.get(i).setSubsections(subsections);
            i++;
        }
    }

    private static List<ForumSubsection> getHotSubsections(Elements titles) {
        List<ForumSubsection> subsections = new ArrayList<ForumSubsection>();
        int i = 0;
        while (i < titles.size()) {
            ForumSubsection forumSubsection = new ForumSubsection();
            forumSubsection.setTitle(titles.get(i).text());
            forumSubsection.setSubsectionId(Long.valueOf(titles.get(i).attr(HREF)));
            subsections.add(forumSubsection);
            i++;
        }
        return subsections;
    }

    private static List<ForumSubsection> getSubsections(Elements titles, Elements descriptions, Elements threadCounts, Elements commentsCounts,
                                                        Elements lastCommentAuthorNames, Elements lastCommentCreationDate) {
        List<ForumSubsection> subsections = new ArrayList<ForumSubsection>();
        int i = 0;
        if (titles.size() == descriptions.size()) {
            while (i < titles.size()) {
                ForumSubsection forumSubsection = new ForumSubsection();
                forumSubsection.setTitle(titles.get(i).text());
                forumSubsection.setSubsectionId(Long.valueOf(titles.get(i).attr(HREF)));
                forumSubsection.setDescription(descriptions.get(i).text());
                forumSubsection.setThreadsCount(Integer.valueOf(threadCounts.get(i).text().replace(SPACE, WITHOUT_SPACE)));
                forumSubsection.setCommentsCount(Integer.valueOf(commentsCounts.get(i).childNode(3).toString().replace(SPACE, WITHOUT_SPACE)));
                forumSubsection.setLastCommentAuthorId(lastCommentAuthorNames.get(i).attr(HREF).replace(USER_PROFILE_LINK, WITHOUT_SPACE));
                String date = lastCommentCreationDate.get(i).attr("title");
                if (date != null) {
                    forumSubsection.setLastCommentCreationDate(getMillisFromDate(date));
                }

                subsections.add(forumSubsection);
                i++;
            }
        }
        return subsections;
    }

    private static Long getMillisFromDate(String date) {
        Locale locale = new Locale("ru");
        String[] a = date.split(" ");
        String currentMonth = a[1];
        String cutMonth = getCorrectMonth(currentMonth);
//        String cutMonth = currentMonth.substring(0, currentMonth.length() - 1);
        a[1] = cutMonth;

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < a.length; i++) {
            sb.append(a[i]).append(" ");
        }
        String resultDate = sb.toString().trim();
        SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy HH:mm", locale);
        Date d = null;
        try {
            d = format.parse(resultDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d.getTime();
    }

    private static String getCorrectMonth(String currentMonth) {
        String cutMonth;
        if (currentMonth.equals("января")) {
            cutMonth = "январь";
        } else if (currentMonth.equals("февраля")) {
            cutMonth = "февраль";
        } else if (currentMonth.equals("марта")) {
            cutMonth = "март";
        } else if (currentMonth.equals("апреля")) {
            cutMonth = "апрель";
        } else if (currentMonth.equals("мая")) {
            cutMonth = "май";
        } else if (currentMonth.equals("июня")) {
            cutMonth = "июнь";
        } else if (currentMonth.equals("июля")) {
            cutMonth = "июль";
        } else if (currentMonth.equals("августа")) {
            cutMonth = "август";
        } else if (currentMonth.equals("сентября")) {
            cutMonth = "сентябрь";
        } else if (currentMonth.equals("октября")) {
            cutMonth = "октябрь";
        } else if (currentMonth.equals("ноября")) {
            cutMonth = "ноябрь";
        } else if (currentMonth.equals("декабря")) {
            cutMonth = "декабрь";
        } else {
            cutMonth = "Invalid month";
        }
        return cutMonth;
    }
}
