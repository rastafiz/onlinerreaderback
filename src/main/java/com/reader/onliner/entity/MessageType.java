package com.reader.onliner.entity;

/**
 * Created by aliakseimitskevich on 10/15/15.
 */
public enum MessageType {
    TEXT, BLOCKQUOTE, CITE;
}
