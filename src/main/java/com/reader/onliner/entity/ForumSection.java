package com.reader.onliner.entity;

import java.util.List;

import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 10.12.13
 * Time: 19:30
 * To change this template use File | Settings | File Templates.
 */

@Component
public class ForumSection
{
  private String title;
  private List<ForumSubsection> subsections;

  public ForumSection()
  {
  }

  public ForumSection(String title, List<ForumSubsection> subsections)
  {
    this.title = title;
    this.subsections = subsections;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public List<ForumSubsection> getSubsections()
  {
    return subsections;
  }

  public void setSubsections(List<ForumSubsection> subsections)
  {
    this.subsections = subsections;
  }

  public static Builder newBuilder()
  {
    return new ForumSection().new Builder();
  }

  public class Builder
  {
    private Builder()
    {
      //private constructor
    }

    public Builder setTitle(String title)
    {
      ForumSection.this.title = title;

      return this;
    }

    public Builder setSubsections(List<ForumSubsection> subsections)
    {
      ForumSection.this.subsections = subsections;
      return this;
    }

    public ForumSection build()
    {
      return ForumSection.this;
    }

  }

}
