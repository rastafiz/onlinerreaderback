package com.reader.onliner.entity;

import java.util.List;

/**
 * Created by Алексей on 15.12.13.
 */
public class ForumCommentsPage {
    private Boolean isClosed;
    private Long currentPage;
    private Long totalPage;
    private List<ForumComment> comments;

    public ForumCommentsPage() {
    }

    public ForumCommentsPage(Boolean isClosed, Long currentPage, Long totalPage, List<ForumComment> comments) {
        this.isClosed = isClosed;
        this.currentPage = currentPage;
        this.totalPage = totalPage;
        this.comments = comments;
    }

    public Long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }

    public Long getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }

    public List<ForumComment> getComments() {
        return comments;
    }

    public void setComments(List<ForumComment> comments) {
        this.comments = comments;
    }

    public Boolean getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(Boolean isClosed) {
        this.isClosed = isClosed;
    }
}
