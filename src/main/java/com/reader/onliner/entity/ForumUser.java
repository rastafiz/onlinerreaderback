package com.reader.onliner.entity;

import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 10.12.13
 * Time: 19:41
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ForumUser {

    private String username;
    private String userForumID;
    private ImageIO userAvatar;
    private String userCity;
    private Date userRegistrationDate;

    public ForumUser() {
    }

    public ForumUser(String username, String userForumID, ImageIO userAvatar, String userCity, Date userRegistrationDate) {
        this.username = username;
        this.userForumID = userForumID;
        this.userAvatar = userAvatar;
        this.userCity = userCity;
        this.userRegistrationDate = userRegistrationDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserForumID() {
        return userForumID;
    }

    public void setUserForumID(String userForumID) {
        this.userForumID = userForumID;
    }

    public ImageIO getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(ImageIO userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public Date getUserRegistrationDate() {
        return userRegistrationDate;
    }

    public void setUserRegistrationDate(Date userRegistrationDate) {
        this.userRegistrationDate = userRegistrationDate;
    }
}
