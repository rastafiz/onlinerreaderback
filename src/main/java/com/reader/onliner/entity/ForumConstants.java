package com.reader.onliner.entity;

/**
 * Created by Алексей on 15.12.13.
 */
public interface ForumConstants {
    public static final String HREF = "href";
    public static final String SPACE = " ";
    public static final String WITHOUT_SPACE = "";
    public static final String USER_PROFILE_LINK = "https://profile.onliner.by/user/";
    public static final String TITLE = "title";

    public static final String FORUM_ONLINER_BY_MAIN_PAGE_URL = "http://forum.onliner.by/";
    public static final String VIEW_FORUM_PREFIX = FORUM_ONLINER_BY_MAIN_PAGE_URL + "viewforum.php?f=";
    public static final String VIEW_THREAD_PREFIX = FORUM_ONLINER_BY_MAIN_PAGE_URL + "viewtopic.php?t=";
}
