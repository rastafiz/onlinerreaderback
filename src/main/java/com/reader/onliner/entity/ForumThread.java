package com.reader.onliner.entity;

import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 10.12.13
 * Time: 19:36
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ForumThread implements ForumConstants {


    private Long threadId;
    private String title;
    private Long authorId;
    private Long lastCommentTimestamp;
    private Long commentsCount;
    private Boolean isSelected;
    private Long pageCount;
    private Long lastCommentAuthorId;

    public ForumThread() {
    }

    public ForumThread(String title, Long threadId, Long authorId, Long lastCommentTimestamp,
                       Long commentsCount, Boolean isSelected, Long pageCount, Long lastCommentAuthorId) {
        this.threadId = threadId;
        this.title = title;
        this.authorId = authorId;
        this.lastCommentTimestamp = lastCommentTimestamp;
        this.commentsCount = commentsCount;
        this.isSelected = isSelected;
        this.pageCount = pageCount;
        this.lastCommentAuthorId = lastCommentAuthorId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getThreadId() {
        return threadId;
    }

    public void setThreadId(Long threadId) {
        this.threadId = threadId;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public Long getLastCommentTimestamp() {
        return lastCommentTimestamp;
    }

    public void setLastCommentTimestamp(Long lastCommentTimestamp) {
        this.lastCommentTimestamp = lastCommentTimestamp;
    }

    public Long getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(Long commentsCount) {
        this.commentsCount = commentsCount;
    }

    public Boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
    }

    public Long getPageCount() {
        return pageCount;
    }

    public void setPageCount(Long pageCount) {
        this.pageCount = pageCount;
    }

    public Long getLastCommentAuthorId() {
        return lastCommentAuthorId;
    }

    public void setLastCommentAuthorId(Long lastCommentAuthorId) {
        this.lastCommentAuthorId = lastCommentAuthorId;
    }








}
