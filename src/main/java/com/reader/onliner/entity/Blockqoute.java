package com.reader.onliner.entity;

/**
 * Created by aliakseimitskevich on 10/15/15.
 */
public class Blockqoute extends SimpleTextMessage
{
    Cite cite;
    Blockqoute blockqoute;

    public Cite getCite() {
        return cite;
    }

    public void setCite(Cite cite) {
        this.cite = cite;
    }

    public Blockqoute getBlockqoute() {
        return blockqoute;
    }

    public void setBlockqoute(Blockqoute blockqoute) {
        this.blockqoute = blockqoute;
    }

}
