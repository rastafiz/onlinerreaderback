package com.reader.onliner.entity;

import java.util.List;

import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: Алексей
 * Date: 10.12.13
 * Time: 19:41
 * To change this template use File | Settings | File Templates.
 */

@Component
public class ForumComment {
    private Long authorId;
    private List<SimpleTextMessage> simpleTextMessageList;
    private Long commentID;
    private Long creationDateTimestamp;

    public ForumComment() {
    }

    public ForumComment(Long commentID, Long authorId, List<SimpleTextMessage> simpleTextMessageList, Long creationDateTimestamp) {
        this.authorId = authorId;
        this.simpleTextMessageList = simpleTextMessageList;
        this.commentID = commentID;
        this.creationDateTimestamp = creationDateTimestamp;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<SimpleTextMessage> getSimpleTextMessageList() {
        return simpleTextMessageList;
    }

    public void setSimpleTextMessageList(List<SimpleTextMessage> simpleTextMessageList) {
        this.simpleTextMessageList = simpleTextMessageList;
    }

    public Long getCommentID() {
        return commentID;
    }

    public void setCommentID(Long commentID) {
        this.commentID = commentID;
    }

    public Long getCreationDateTimestamp() {
        return creationDateTimestamp;
    }

    public void setCreationDateTimestamp(Long creationDateTimestamp) {
        this.creationDateTimestamp = creationDateTimestamp;
    }

}
