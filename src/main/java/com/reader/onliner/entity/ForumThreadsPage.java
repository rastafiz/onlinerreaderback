package com.reader.onliner.entity;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Алексей on 14.12.13.
 */
@Component
public class ForumThreadsPage {
    Long currentPage;
    Long totalPage;
    List<ForumThread> threads;

    public ForumThreadsPage() {
    }

    public ForumThreadsPage(Long currentPage, Long totalPage, List<ForumThread> threads) {
        this.currentPage = currentPage;
        this.totalPage = totalPage;
        this.threads = threads;
    }

    public Long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }

    public Long getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }

    public List<ForumThread> getThreads() {
        return threads;
    }

    public void setThreads(List<ForumThread> threads) {
        this.threads = threads;
    }
}
