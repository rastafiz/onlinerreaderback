package com.reader.onliner.entity;

import org.springframework.stereotype.Component;

/**
 * Created by aliakseimitskevich on 10/15/15.
 */
@Component
public class SimpleTextMessage
{
    private MessageType type;
    private String content;

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
