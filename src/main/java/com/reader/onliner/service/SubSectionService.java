package com.reader.onliner.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import com.reader.onliner.entity.ForumConstants;
import com.reader.onliner.entity.ForumSection;
import com.reader.onliner.entity.ForumSubsection;

/**
 * Created by aliakseimitskevich on 10/14/15.
 */
@Service
public class SubSectionService implements ForumConstants
{

  private static final String SUBSECTIONS_LIST = "div.b-hdtopic ~ ul.b-list-topics";
  private static final String HOT_SUBSECTION_TITLES = "li > h3 > a";
  private static final String SUBSECTION_TITLES = "li > div > h3 > a";
  private static final String SUBSECTION_DESCRIPTIONS = "li > div > p";
  private static final String THREAD_COUNTS = "li > div > div > strong";
  private static final String COMMENT_COUNTS = "li > div > div";
  private static final String LAST_COMMENT_AUTHOR_NAMES = "li > div.b-lt-author > a.dark-gray";
  private static final String LAST_COMMENT_CREATION_DATES = "li > div.b-lt-author > a.link-getlast";

  public void completeSectionsSubsections(Document doc, List<ForumSection> sectionsList)
  {
    Elements elements;
    elements = doc.select(SUBSECTIONS_LIST);
    int i = 0;
    for (Element element : elements)
    {
      List<ForumSubsection> subsections = new ArrayList<ForumSubsection>();
      Elements hotSubsectionTitles = element.select(HOT_SUBSECTION_TITLES);
      subsections = getHotSubsections(hotSubsectionTitles);
      if (subsections.size() == 0)
      {
        Elements subsectionsTitles = element.select(SUBSECTION_TITLES);
        Elements subsectionsDescriptions = element.select(SUBSECTION_DESCRIPTIONS);
        Elements threadCounts = element.select(THREAD_COUNTS);
        Elements commentsCounts = element.select(COMMENT_COUNTS);
        Elements lastCommentAuthorNames = element.select(LAST_COMMENT_AUTHOR_NAMES);
        Elements lastCommentCreationDate = element.select(LAST_COMMENT_CREATION_DATES);
        subsections = getSubsections(subsectionsTitles, subsectionsDescriptions, threadCounts, commentsCounts,
            lastCommentAuthorNames,
            lastCommentCreationDate);
      }
      sectionsList.get(i).setSubsections(subsections);
      i++;
    }
    sectionsList.remove(0); // Delete hot sections
  }

  public List<ForumSection> eraseEmptySubsections(List<ForumSection> sectionsList)
  {
    return sectionsList.parallelStream().filter(s -> s.getSubsections() != null).collect(Collectors.toList());
  }

  private List<ForumSubsection> getHotSubsections(Elements titles)
  {
    List<ForumSubsection> subsections = new ArrayList<ForumSubsection>();
    int i = 0;
    while (i < titles.size())
    {
      ForumSubsection forumSubsection = new ForumSubsection();
      forumSubsection.setTitle(titles.get(i).text());
      forumSubsection.setSubsectionId(Long.valueOf(titles.get(i).attr(HREF).replace("./viewtopic.php?t=", "")));
      subsections.add(forumSubsection);
      i++;
    }
    return subsections;
  }

  private List<ForumSubsection> getSubsections(Elements titles, Elements descriptions, Elements threadCounts,
      Elements commentsCounts,
      Elements lastCommentAuthorNames, Elements lastCommentCreationDate)
  {
    List<ForumSubsection> subsections = new ArrayList<ForumSubsection>();
    int i = 0;
    if (titles.size() == descriptions.size())
    {
      while (i < titles.size())
      {
        ForumSubsection forumSubsection = new ForumSubsection();
        forumSubsection.setTitle(titles.get(i).text());
        forumSubsection.setSubsectionId(Long.valueOf(titles.get(i).attr(HREF).replace("./viewforum.php?f=", "")));
        forumSubsection.setDescription(descriptions.get(i).text());
        forumSubsection.setThreadsCount(Integer.valueOf(threadCounts.get(i).text().replace(SPACE, WITHOUT_SPACE)));
        forumSubsection.setCommentsCount(
            Integer.valueOf(commentsCounts.get(i).childNode(3).toString().replace(SPACE, WITHOUT_SPACE)));
        forumSubsection
            .setLastCommentAuthorId(lastCommentAuthorNames.get(i).attr(HREF).replace(USER_PROFILE_LINK, WITHOUT_SPACE));
        String date = lastCommentCreationDate.get(i).attr("title");
        if (date != null)
        {
          forumSubsection.setLastCommentCreationDate(getMillisFromDate(date));
        }

        subsections.add(forumSubsection);
        i++;
      }
    }
    return subsections;
  }

  private Long getMillisFromDate(String date)
  {
    Locale locale = new Locale("ru");
    String[] a = date.split(" ");
    String currentMonth = a[1];
    String cutMonth = getCorrectMonth(currentMonth);
    //        String cutMonth = currentMonth.substring(0, currentMonth.length() - 1);
    a[1] = cutMonth;

    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < a.length; i++)
    {
      sb.append(a[i]).append(" ");
    }
    String resultDate = sb.toString().trim();
    SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy HH:mm", locale);
    Date d = null;
    try
    {
      d = format.parse(resultDate);
    }
    catch (ParseException e)
    {
      e.printStackTrace();
    }
    return d.getTime();
  }

  private String getCorrectMonth(String currentMonth)
  {
    String cutMonth;
    if (currentMonth.equals("января"))
    {
      cutMonth = "январь";
    }
    else if (currentMonth.equals("февраля"))
    {
      cutMonth = "февраль";
    }
    else if (currentMonth.equals("марта"))
    {
      cutMonth = "март";
    }
    else if (currentMonth.equals("апреля"))
    {
      cutMonth = "апрель";
    }
    else if (currentMonth.equals("мая"))
    {
      cutMonth = "май";
    }
    else if (currentMonth.equals("июня"))
    {
      cutMonth = "июнь";
    }
    else if (currentMonth.equals("июля"))
    {
      cutMonth = "июль";
    }
    else if (currentMonth.equals("августа"))
    {
      cutMonth = "август";
    }
    else if (currentMonth.equals("сентября"))
    {
      cutMonth = "сентябрь";
    }
    else if (currentMonth.equals("октября"))
    {
      cutMonth = "октябрь";
    }
    else if (currentMonth.equals("ноября"))
    {
      cutMonth = "ноябрь";
    }
    else if (currentMonth.equals("декабря"))
    {
      cutMonth = "декабрь";
    }
    else
    {
      cutMonth = "Invalid month";
    }
    return cutMonth;
  }
}
