package com.reader.onliner.service;

import com.reader.onliner.entity.ForumConstants;
import com.reader.onliner.entity.ForumThread;
import com.reader.onliner.utils.CommonForumUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aliakseimitskevich on 10/15/15.
 */
@Service
public class ThreadService implements ForumConstants {

    public static final String THREADS_LIST = "div.h-list-subjs > ul > li";
    public static final String LAST_COMMENT_DATE = "div.b-lt-author > a.link-getlast";
    public static final String LAST_COMMENT_AUTHOR = "div.b-lt-author > a.dark-gray";
    public static final String COMMENTS_PAGES = "div.b-lt-subj > span.b-navonsubj > a";
    public static final String TOTAL_MESSAGES = "strong.total-msg";
    public static final String TOPIC_STARTER = "div.b-lt-subj > span > big > a.gray";
    public static final String TOPIC_TITLE = "div.b-lt-subj > h3 > a.topictitle";
    public static final String VIEWTOPIC_PREFIX = "./viewtopic.php?t=";

    public List<ForumThread> getThreads(Document doc) {
        List<ForumThread> threadList = new ArrayList<ForumThread>();
        Elements threads = doc.select(THREADS_LIST);
        for (Element thread : threads) {
            Long id = getForumId(thread);
            String title = getTitle(thread);
            Long authorId = getThreadAuthorId(thread);
            Long commentsCount = getCommentsCount(thread);
            Boolean isSelected = isSelected(thread);
            Long pageCount = getPageCountWithMessages(thread);
            Long lastCommentAuthorId = getLastCommentAuthorId(thread);
            String lastCommentDate = getLastCommentDate(thread);
            Long lastCommentTimestamp = CommonForumUtils.getMillisFromDate(lastCommentDate);
            ForumThread forumThread =
                    new ForumThread(title, id, authorId, lastCommentTimestamp, commentsCount, isSelected, pageCount, lastCommentAuthorId);
            threadList.add(forumThread);
        }
        return threadList;
    }

    private Long getForumId(Element thread) {
        return Long.valueOf(thread.select(TOPIC_TITLE).attr(HREF).replace(VIEWTOPIC_PREFIX, WITHOUT_SPACE));
    }

    private String getLastCommentDate(Element thread) {
        return thread.select(LAST_COMMENT_DATE).attr(TITLE);
    }

    private Long getLastCommentAuthorId(Element thread) {
        return Long.valueOf(thread.select(LAST_COMMENT_AUTHOR).attr(HREF).replace(USER_PROFILE_LINK, WITHOUT_SPACE));
    }

    private Long getPageCountWithMessages(Element thread) {
        Long pageCount = null;
        if (thread.select(COMMENTS_PAGES).size() != 0) {
            Elements pageLinks = thread.select(COMMENTS_PAGES);
            pageCount = Long.valueOf(pageLinks.get(pageLinks.size() - 1).text());
        } else {
            pageCount = 1L;
        }
        return pageCount;
    }

    private boolean isSelected(Element thread) {
        return thread.className().isEmpty() ? false : true;
    }

    private Long getCommentsCount(Element thread) {
        return Long.valueOf(thread.select(TOTAL_MESSAGES).text());
    }

    private Long getThreadAuthorId(Element thread) {
        return Long.valueOf(thread.select(TOPIC_STARTER).attr(HREF).replace(USER_PROFILE_LINK, WITHOUT_SPACE));
    }

    private String getTitle(Element thread) {
        return thread.select(TOPIC_TITLE).text();
    }

}
