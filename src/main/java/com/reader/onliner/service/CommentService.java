package com.reader.onliner.service;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.bind.annotation.RestController;

import com.reader.onliner.entity.Blockqoute;
import com.reader.onliner.entity.Cite;
import com.reader.onliner.entity.ForumComment;
import com.reader.onliner.entity.SimpleTextMessage;
import com.reader.onliner.entity.MessageType;
import com.reader.onliner.utils.CommonForumUtils;

/**
 * Created by aliakseimitskevich on 10/15/15.
 */
@RestController
public class CommentService {

    public List<ForumComment> getComments(Document doc) {
        List<ForumComment> commentsList = new ArrayList<ForumComment>();
        Elements comments = doc.select("ul.b-messages-thread > li[id]");
        for (Element comment : comments) {
            List<SimpleTextMessage> messagesList = new ArrayList<>();
            Long commentID = getCommentId(comment);
            Long authorId = getAuthorId(comment);
            getText(comment, messagesList);
            String creationDate = getCreationDate(comment);
            Long creationDateTimestamp = CommonForumUtils.getMillisFromDate(creationDate);
            ForumComment forumComment = new ForumComment(commentID, authorId, messagesList, creationDateTimestamp);
            commentsList.add(forumComment);
        }
        return commentsList;
    }

    private String getCreationDate(Element comment) {
        return comment.select("div.b-msgpost-txt > div.msgpost-txt-i > small.msgpost-date > span").get(0).text();
    }

    private void getText(Element comment, List<SimpleTextMessage> messagesList) {
        Elements select = comment.select("div.b-msgpost-txt > div.msgpost-txt-i > div.content");
        StringBuilder sb = new StringBuilder();
        for (Element element : select) {
            for (Element child : element.children()) {
                if (child.tagName().equals("blockquote")) {
                    messagesList.add(processBlockquote(child));
                }

                if (child.tagName().equals("p")) {
                    messagesList.add(processMessage(child));
                }
            }
        }

    }

    private Blockqoute processBlockquote(Element child) {
        Blockqoute b = new Blockqoute();
        for (Element c : child.children()) {
            if (c.tagName().equals("cite")) {
                b.setCite(processCite(c));
            }

            if (c.tagName().equals("blockquote")) {
                b.setBlockqoute(processBlockquote(c));
            }

            if (c.tagName().equals("p")) {
                b.setContent(c.text());
            }
        }
        b.setType(MessageType.BLOCKQUOTE);
        return b;
    }

    private Cite processCite(Element c) {
        Cite cite = new Cite();
        cite.setType(MessageType.CITE);
        cite.setContent(c.text());
        return cite;
    }

    private SimpleTextMessage processMessage(Element child) {
        SimpleTextMessage m = new SimpleTextMessage();
        m.setType(MessageType.TEXT);
        m.setContent(child.text());
        return m;
    }

//    private void processChild(Element child, List<Message> messagesList) {
//        Message m = new Message();
//        if (child.children().size() != 0) {
//            for (Element c : child.children()) {
//                processChild(c, messagesList);
//            }
//        }
//        if (child.tagName().equals("p")) {
//            m.setType(child.tagName());
//            m.setContent(child.text());
//            messagesList.add(m);
//        }
//
//    }

    private Long getAuthorId(Element comment) {
        return Long.valueOf(comment.select("div.b-mtauthor").attr("data-user_id"));
    }

    private Long getCommentId(Element comment) {
        return Long.valueOf(comment.attr("id").replace("p", ""));
    }
}
