package com.reader.onliner.service;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reader.onliner.entity.ForumSection;

/**
 * Created by aliakseimitskevich on 10/14/15.
 */
@Service
public class SectionService
{

  @Autowired
  private SubSectionService subSectionService;

  private static final String SECTION_LIST = "div.b-hdtopic > h2";

  public List<ForumSection> createSectionsList(Document doc)
  {
    Elements elements = doc.select(SECTION_LIST);
    List<ForumSection> sectionsList = new ArrayList<ForumSection>();
    for (Element element : elements)
    {
      sectionsList.add(ForumSection.newBuilder().setTitle(element.text()).build());
    }

    subSectionService.completeSectionsSubsections(doc, sectionsList);
    sectionsList = subSectionService.eraseEmptySubsections(sectionsList);

    return sectionsList;
  }
}
