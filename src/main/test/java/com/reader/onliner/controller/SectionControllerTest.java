package com.reader.onliner.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.reader.onliner.Application;
import com.reader.onliner.service.SectionService;

/**
 * @author alexey.mitskevich
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class SectionControllerTest
{
  private MockMvc mockMvc;

  @Autowired
  private SectionService sectionServiceMock;

  //Add WebApplicationContext field here.

  //The setUp() method is omitted.

  @Test
  public void findAll_TodosFound_ShouldReturnFoundTodoEntries() throws Exception
  {
//    Todo first = new TodoBuilder()
//        .id(1L)
//        .description("Lorem ipsum")
//        .title("Foo")
//        .build();
//    Todo second = new TodoBuilder()
//        .id(2L)
//        .description("Lorem ipsum")
//        .title("Bar")
//        .build();
//
//    when(sectionServiceMock.createSectionsList()).thenReturn(Arrays.asList(first, second));
//
//    mockMvc.perform(get("/api/todo"))
//        .andExpect(status().isOk())
//        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
//        .andExpect(jsonPath("$", hasSize(2)))
//        .andExpect(jsonPath("$[0].id", is(1)))
//        .andExpect(jsonPath("$[0].description", is("Lorem ipsum")))
//        .andExpect(jsonPath("$[0].title", is("Foo")))
//        .andExpect(jsonPath("$[1].id", is(2)))
//        .andExpect(jsonPath("$[1].description", is("Lorem ipsum")))
//        .andExpect(jsonPath("$[1].title", is("Bar")));
//
//    verify(todoServiceMock, times(1)).findAll();
//    verifyNoMoreInteractions(todoServiceMock);
  }
}
