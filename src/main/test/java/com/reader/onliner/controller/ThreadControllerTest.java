package com.reader.onliner.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.reader.onliner.Application;

/**
 * @author alexey.mitskevich
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ThreadControllerTest
{
  @Test
  public void test()
  {

  }
}
